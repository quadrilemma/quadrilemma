const EventEmitter = require('events')
const event = new EventEmitter();

const ac = require('./ethereum_implementations/ComAuctioneer')
const bp = require('./ethereum_implementations/ComBidderPool')

const bidderPool = new bp(event);
const auctioneer = new ac(event);

auctioneer.newAuctionRequest();