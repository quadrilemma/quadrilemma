
const env = require("../environment.js");
const bidderConf = require('./ComBidder');
const Web3 = require('web3');
const ConfidentialAuction = require('./ConfidentialAuction.json')
const N = 2;

module.exports = function (event) {

    const web3ws = new Web3(new Web3.providers.WebsocketProvider(env.InfuraProviderWSS));

    console.log("Start BidderPool");
    let bidderPool = [];

    for (let i = 0; i < N; i++) {
        bidderPool.push(new bidderConf(web3ws, i));
    }

    event.on('new_auction', (address, deposit, revealPeriod)=>{
        try{
            console.log("RICEVUTO");
            for (let i = 0; i < N; i++) {
                bidderPool[i].sendBid(address, revealPeriod, deposit);
            }
            setTimeout(() => {
                web3ws.currentProvider.disconnect();
            }, 240000);
        } catch (e) {
            console.log(e);
            web3ws.currentProvider.disconnect();
        }
    });


}

