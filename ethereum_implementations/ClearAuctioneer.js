const env = require("../environment.js");
const {toBuffer} = require("ethereumjs-util");
const Web3 = require('web3');
const {asciiToHex, randomHex} = require("web3-utils");
const OpenAuction = require('./OpenAuction.json')

module.exports = function (event) {


    const web3 = new Web3(new Web3.providers.HttpProvider(env.InfuraUrlProviderHTTPS));
    const web3ws = new Web3(new Web3.providers.WebsocketProvider(env.InfuraProviderWSS));

    const sk = env.ethereum.auctioneer.sK;
    const account = web3.eth.accounts.privateKeyToAccount(sk);
    console.log("Auctioneer address: " + account.address);
    const biddingPeriod = 120;

    const wallet = web3ws.eth.accounts.wallet.add(account);
    const keystore = wallet.encrypt(randomHex(32));


    let OpenAuctionContract;

    this.newAuctionRequest =  async function () {
        newAuction();
    }


    function newAuction(){

        OpenAuctionContract = new web3ws.eth.Contract(OpenAuction.abi);
        OpenAuctionContract.options.data = '0x'+OpenAuction.bytecode.object;

        let newDeploy = OpenAuctionContract.deploy({arguments: [biddingPeriod, account.address]})
        newDeploy.estimateGas({from: account.address},(err, gas) =>{
            if (err) console.log(err);

            newDeploy.send({
                from: account.address,
                gas: Math.floor(gas*1.5),
            })
            .once('transactionHash', txhash => {
                console.log(`Mining ContractCreation transaction ...`);
                console.log(`https://ropsten.etherscan.io/tx/${txhash}`);
            })
            .then(function(newContractInstance) {
                OpenAuctionContract = new web3ws.eth.Contract(OpenAuction.abi, newContractInstance.options.address)// instance with the new contract address
                console.log(OpenAuctionContract.options.address);
                console.log("============== NEW EVENT ==============");
                event.emit('new_auction', OpenAuctionContract.options.address, biddingPeriod);
                console.log("============== SET TIMEOUT ==============");
                let interval = setInterval(() => {
                    const newReq =  OpenAuctionContract.methods.auctionEnd();
                    newReq.estimateGas({from: account.address}).then((gas)=> {
                        newReq.send({from: account.address,gas: Math.trunc(gas*(1.5))}).
                        once('transactionHash', txhash => {
                            console.log(`Mining auctionEnd transaction ...`);
                            console.log(`https://ropsten.etherscan.io/tx/${txhash}`);
                        }).then((r) => {
                            console.log("Highest bid is " + r.events.AuctionEnded.returnValues.winner + "with the value: "+ r.events.AuctionEnded.returnValues.amount);
                            clearInterval(interval);
                            web3ws.currentProvider.disconnect();
                        }).catch((e) => {
                            console.log(e)
                            web3ws.currentProvider.disconnect()});
                    });
                }, (biddingPeriod+20)*1000);

            }).catch((e) => {
                        console.log(e)
                        web3ws.currentProvider.disconnect()})
        });

    }
}