const EventEmitter = require('events')
const event = new EventEmitter();

const ac = require('./algorand_implementations/ComAuctioneer')
const bp = require('./algorand_implementations/ComBidderPool')

const bidderPool = new bp(event);
const auctioneer = new ac(event);


auctioneer.startingAuction();