const env = require("../environment.js");
const {asciiToHex, randomHex} = require("web3-utils");
const OpenAuction = require('./OpenAuction.json')

module.exports = function (web3ws, i) {

    const sk = env.ethereum.bidder[i];
    const account = web3ws.eth.accounts.privateKeyToAccount(sk);


    const wallet = web3ws.eth.accounts.wallet.add(account);

    let OpenAuctionContract;
    let retrieved = false;

    this.getAddress = function (){
        console.log("Bidder", i+"'s", "address:", account.address);
    }

    this.sendBid = function (address, ind){
        OpenAuctionContract = new web3ws.eth.Contract(OpenAuction.abi, address);
        const myBid = Math.floor( (ind + 1) * 1000);

        console.log("Bid is:", myBid*10000);
        const newReq =  OpenAuctionContract.methods.bid();
        newReq.estimateGas({from: account.address, value: myBid*10000}).then((gas)=> {
            newReq.send({from: account.address,gas: Math.trunc(gas*(1.5)), value: myBid*10000}).
            once('transactionHash', txhash => {
                console.log(`Mining newSend transaction ...`);
                console.log(`https://ropsten.etherscan.io/tx/${txhash}`);
            }).then((r) => {
                OpenAuctionContract.events.HighestBidIncreased().on('data', (event) => {
                    console.log(event.returnValues.bidder);
                    /*if (event.returnValues.bidder !== account.address && !retrieved){
                        const withdraw =  OpenAuctionContract.methods.withdraw();
                        withdraw.estimateGas({from: account.address, value: myBid*10000 }).then((gas)=> {
                            withdraw.send({from: account.address,gas: Math.trunc(gas*(1.5))}).
                            once('transactionHash', txhash => {
                                console.log(`Mining withdraw transaction ...`);
                                console.log(`https://ropsten.etherscan.io/tx/${txhash}`);
                            }).then((r) => {
                                console.log("Bidder " + account.address +" retrieves its money!")
                                retrieved = true;
                            });
                        });
                    }*/
                });
            });
        });
    }
}