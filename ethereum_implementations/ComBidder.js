const env = require("../environment.js");
const {asciiToHex, soliditySha3} = require("web3-utils");
const {toBuffer} = require("ethereumjs-util");


const ConfidentialAuction = require('./ConfidentialAuction.json')

module.exports = function (web3ws, i) {

    const sk = env.ethereum.bidder[i];
    const account = web3ws.eth.accounts.privateKeyToAccount(sk);
    const myBid = Math.floor( (i + 1) * 1000)*10000;
    const hashBid = web3ws.utils.soliditySha3({t: 'uint256', v: myBid});

    const wallet = web3ws.eth.accounts.wallet.add(account);

    let ConfidentialAuctionContract;
    let retrieved = false;

    this.getAddress = function (){
        console.log("Bidder", i+"'s", "address:", account.address);
    }

    this.sendBid = function (address, period, deposit){
        try{
            ConfidentialAuctionContract = new web3ws.eth.Contract(ConfidentialAuction.abi, address);
            console.log("Commitment is:", hashBid);
            const newReq =  ConfidentialAuctionContract.methods.bid(hashBid);
            newReq.estimateGas({from: account.address, value: deposit}).then((gas)=> {
                newReq.send({from: account.address,gas: Math.trunc(gas*(1.5)), value: deposit})
                    .once('transactionHash', txhash => {
                        console.log(`Mining bid transaction ...`);
                        console.log(`https://ropsten.etherscan.io/tx/${txhash}`);
                    }).then((r) => {
                    setTimeout(() => {
                        const newRev =  ConfidentialAuctionContract.methods.reveal();
                        newRev.estimateGas({from: account.address, value: myBid}).then((gas)=> {
                            newRev.send({from: account.address, gas: Math.trunc(gas*(1.5)), value: myBid})
                                .once('transactionHash', txhash => {
                                    console.log(`Mining reveal transaction ...`);
                                    console.log(`https://ropsten.etherscan.io/tx/${txhash}`);
                                }).then((r) => {
                                /* ConfidentialAuctionContract.events.HighestBidIncreased().on('data', (event) => {
                                    console.log(event.returnValues.bidder);
                                    if (event.returnValues.bidder !== account.address && !retrieved){
                                        const withdraw =  ConfidentialAuctionContract.methods.withdraw();
                                        withdraw.estimateGas({from: account.address, value: myBid }).then((gas)=> {
                                            withdraw.send({from: account.address,gas: Math.trunc(gas*(1.5))}).
                                            once('transactionHash', txhash => {
                                                console.log(`Mining withdraw transaction ...`);
                                                console.log(`https://ropsten.etherscan.io/tx/${txhash}`);
                                            }).then((r) => {
                                                console.log("Bidder " + account.address +" retrieves its money!")
                                                retrieved = true;
                                            });
                                        });
                                    }
                                }); */
                            }).catch((e) => {
                                console.log(e);
                                web3ws.currentProvider.disconnect();
                            });
                        });
                    }, (period+10)*1000);
                });
            });

        } catch (e) {
            console.log(e);
            web3ws.currentProvider.disconnect();
        }
    }
}