const algosdk = require('algosdk');
const fs = require('fs');

const env = require("../environment.js");
const utils = require('./utils');

module.exports = function (event) {

    const algodclient = new algosdk.Algodv2(env.token, env.baseServer, env.port);

    let account = algosdk.mnemonicToSecretKey(env.auctioneer.mnemonic);
    console.log( "My address: " + account.addr);

    let escrow = '';
    let escrowLogicSignature = undefined;
    let assetID = 14080144;
    let appID = 14080380;
    let commitAppIDs = [];
    let endAuction = 0;
    let endBidding = 0;
    let startAuction = 0;

    this.startingAuction = async function (){
        // await utils.readGlobalState(algodclient, account, 14089677);
        await createAsset();
        await deployAuction();
    }

    function computeEndAuction(start, min) {
        return start + min * 300;
    }

    function computeEndBidding(start, min) {
        return start + min * 300;
    }

    async function createAsset() {
        try {
            let params = await algodclient.getTransactionParams().do();
            //comment out the next two lines to use suggested fee
            params.fee = 1000;
            params.flatFee = true;

            let data = {
                "from": account.addr,
                "suggestedParams": params,
                "total": 1,
                "defaultFrozen": false,
                "unitName": 'CAR',
                "assetName": 'AUCTION',
                "manager": account.addr,
                "reserve": account.addr,
                "freeze": account.addr,
                "clawback": account.addr,
                "url": '',
                "decimals": 0
            };

            // signing and sending "txn" allows "addr" to create an asset
            let txn = algosdk.makeAssetCreateTxnWithSuggestedParamsFromObject(data);

            let rawSignedTxn = algosdk.signTransaction(txn, account.sk);
            let tx = (await algodclient.sendRawTransaction(rawSignedTxn.blob).do());
            console.log("CREATEASSET - Transaction : " + tx.txId);
            // wait for transaction to be confirmed
            await utils.waitForConfirmation(algodclient, tx.txId);
            // Get the new asset's information from the creator account
            let ptx = await algodclient.pendingTransactionInformation(tx.txId).do();
            assetID = ptx["asset-index"];
            // console.log("AssetID = " + assetID);

            await utils.printCreatedAsset(algodclient, account.addr, assetID);
        } catch (e) {
            console.log(e);
        }


    }

    async function deployAuction() {
        console.log("===== DEPLOY AUCTION CONTRACT =====")
        let localInts = 0;
        let localBytes = 0;
        let globalInts = 59;
        let globalBytes = 5;
        try {
            // get node suggested parameters
            let params = await algodclient.getTransactionParams().do();
            // comment out the next two lines to use suggested fee
            params.fee = 1000;
            params.flatFee = true;

            // declare onComplete as NoOp
            let onComplete = algosdk.OnApplicationComplete.NoOpOC;

            //read the approval_file
            fs.readFile(env.basePath+'/pyteal/conf_application_stateful.teal', 'utf8', (err, dataApproval) => {
                if (err) throw err;
                utils.compileProgram(algodclient, dataApproval).then((approvalProgram) => {
                    fs.readFile(env.basePath + '/pyteal/conf_clear_state.teal', 'utf8', (err, dataClear) => {
                        if (err) throw err;
                        utils.compileProgram(algodclient, dataClear).then((clearProgram) => {
                            let appArgs = [];
                            startAuction = params.firstRound;
                            endBidding = computeEndBidding(startAuction, 2)
                            endAuction = computeEndAuction(startAuction, 4);
                            appArgs.push(utils.createUIntForContract((assetID)));

                            // create unsigned transaction
                            let txn = algosdk.makeApplicationCreateTxn(account.addr, params, onComplete,
                                approvalProgram, clearProgram, localInts, localBytes, globalInts, globalBytes, appArgs);
                            let txId = txn.txID().toString();

                            // Sign the transaction
                            let signedTxn = txn.signTxn(account.sk);
                            console.log("DEPLOYSTATEFULCONTRACT - Signed transaction with txID: %s", txId);

                            // Submit the transaction
                            algodclient.sendRawTransaction(signedTxn).do().then(()=>{
                                utils.waitForConfirmation(algodclient, txId).then(async ()=>{
                                    algodclient.pendingTransactionInformation(txId).do().then((transactionResponse)=>{
                                        appID = transactionResponse['application-index'];
                                        console.log("DEPLOYSTATEFULCONTRACT -Created new app-id: ",appID);

                                        let localInts = 0;
                                        let localBytes = 0;
                                        let globalInts = 1;
                                        let globalBytes = 63;

                                        fs.readFile(env.basePath+'/pyteal/committing_application_stateful.teal', 'utf8', (err, dataApproval) => {
                                            if (err) throw err;
                                            utils.compileProgram(algodclient, dataApproval).then((approvalProgram) => {
                                                fs.readFile(env.basePath + '/pyteal/committing_clear_state.teal', 'utf8', (err, dataClear) => {
                                                    if (err) throw err;
                                                    utils.compileProgram(algodclient, dataClear).then((clearProgram) => {
                                                        utils.readGlobalState(algodclient, account.addr, appID).then((variables) => {
                                                            let appArgs = [];
                                                            appArgs.push(utils.createUIntForContract(appID))
                                                            // appArgs.push(algosdk.encodeObj(appID.toString()))

                                                            // create unsigned transaction
                                                            let txn = algosdk.makeApplicationCreateTxn(account.addr, params, onComplete,
                                                                approvalProgram, clearProgram, localInts, localBytes, globalInts, globalBytes, appArgs);
                                                            let txId = txn.txID().toString();

                                                            // Sign the transaction
                                                            let signedTxn = txn.signTxn(account.sk);

                                                            console.log("DEPLOYSTATEFULCONTRACT for COMMMITMENT - Signed transaction with txID: %s", txId);

                                                            // Submit the transaction
                                                            algodclient.sendRawTransaction(signedTxn).do().then(()=>{
                                                                utils.waitForConfirmation(algodclient, txId).then(async ()=>{
                                                                    algodclient.pendingTransactionInformation(txId).do().then(async (transactionResponse)=>{
                                                                        commitAppIDs.push(transactionResponse['application-index']);
                                                                        console.log("DEPLOYSTATEFULCONTRACT -Created new app-id: ", commitAppIDs[0]);
                                                                        await  utils.readGlobalState(algodclient, account.addr, commitAppIDs[0]);

                                                                        // await utils.deleteApp(algodclient, account, appID);
                                                                        // await utils.deleteApp(algodclient, account, commitAppIDs[0])

                                                                        await deployAuctionEscrowContract();
                                                                    }).catch(async (e)=>{
                                                                        console.log(e);
                                                                        await utils.deleteApp(algodclient, account, appID);
                                                                        await utils.deleteApp(algodclient, account, commitAppIDs[0]);
                                                                    })
                                                                })
                                                            }).catch(async (e)=>{
                                                                console.log(e);
                                                                await utils.deleteApp(algodclient, account, appID);
                                                                await utils.deleteApp(algodclient, account, commitAppIDs[0]);
                                                            })

                                                        }).catch(async (e)=>{
                                                            console.log(e);
                                                            await utils.deleteApp(algodclient, account, appID);
                                                            await utils.deleteApp(algodclient, account, commitAppIDs[0]);
                                                        });
                                                    })
                                                })
                                            })
                                        })
                                    })
                                });
                            }).catch((e) =>{
                                console.log(e);
                            });
                        }).catch((e) =>{
                            console.log(e);
                        });
                    });
                });
            });
        } catch (e) {
            console.log(e);
            console.trace();
            await utils.deleteApp(algodclient, account, appID);
            await utils.deleteApp(algodclient, account, commitAppIDs[0]);
        }

    }

    async function deployAuctionEscrowContract(){
        let params = await algodclient.getTransactionParams().do();
        // comment out the next two lines to use suggested fee
        params.fee = 1000;
        params.flatFee = true;

        let runPy = new Promise(function(success, nosuccess) {

            const { spawn } = require('child_process');
            const pyprog = spawn('python3', [env.basePath+'/pyteal/escrow.py', account.addr, assetID, appID]);

            pyprog.stdout.on('data', function(data) {
                success(data);
            });

            pyprog.stderr.on('data', (data) => {
                nosuccess(data);
            });
        });

        runPy.then(async function(fromRunpy) {
            // get suggested parameters

            fs.readFile(env.basePath+'/pyteal/escrow_conf.teal', 'utf8', (err, data) => {
                if (err) throw err;
                utils.compileProgram(algodclient, data).then(async (program) => {
                    let lsig = algosdk.makeLogicSig(program);
                    escrowLogicSignature = lsig;
                    escrow = lsig.address();

                    utils.makePayTransaction(algodclient, account.addr, account.sk, lsig.address()).then(async () => {
                        let params = await algodclient.getTransactionParams().do();
                        // comment out the next two lines to use suggested fee
                        params.fee = 1000;
                        params.flatFee = true;
                        let txn = algosdk.makeAssetTransferTxnWithSuggestedParams(escrow, escrow,
                            undefined, undefined, 0,undefined, assetID, params, undefined);
                        // Create the LogicSigTransaction with contract account LogicSig
                        let rawSignedTxn = algosdk.signLogicSigTransactionObject(txn, lsig);

                        // send raw LogicSigTransaction to network
                        let tx = (await algodclient.sendRawTransaction(rawSignedTxn.blob).do());
                        console.log("ESCROWOPTIN - Transaction : " + tx.txId);
                        await utils.waitForConfirmation(algodclient, tx.txId);


                        // transfer the asset to the escrow
                        await utils.tranferAsset(algodclient, account, escrow, assetID, 1);
                        await utils.printAssetHolding(algodclient, escrow, assetID);

                        console.log("Update escrow account address in stateful contract ...")
                        let appArgs = []
                        appArgs.push(new Uint8Array(Buffer.from('escrow')));
                        appArgs.push(algosdk.decodeAddress(escrow).publicKey);
                        await utils.callApp(algodclient, account, appID, appArgs);

                        console.log("Update scID for commitment in stateful contract ...")
                        appArgs = []
                        appArgs.push(new Uint8Array(Buffer.from('sc')));
                        appArgs.push(new Uint8Array(Buffer.from(commitAppIDs[0].toString())));
                        appArgs.push(utils.createUIntForContract(commitAppIDs[0]));

                        await utils.callApp(algodclient, account, appID, appArgs);

                        console.log("==============NOW SEND AN EVENT TO BIDDERS=========")
                        event.emit('new_auction', assetID, appID, commitAppIDs[0], escrowLogicSignature, account.addr);
                        console.log("==============START TIMER TO END THE AUCTION=========")
                        setTimeout(async () => {
                            let variables = await utils.readGlobalState(algodclient, account.addr, appID);
                            event.emit('winner', variables['MaxBidder']);
                        }, 240000);

                        setTimeout(async () => {
                            await utils.deleteApp(algodclient,account, appID);
                            await utils.deleteApp(algodclient, account, commitAppIDs[0]);
                        }, 300000);

                    }).catch(async (e) => {
                        console.log(e);
                        await utils.deleteApp(algodclient,account, appID);
                        await utils.deleteApp(algodclient, account, commitAppIDs[0]);
                    });
                });
            });

        }).catch((e) => {console.log(e.toString())});
    }




}