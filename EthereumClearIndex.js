const EventEmitter = require('events')
const event = new EventEmitter();

const ac = require('./ethereum_implementations/ClearAuctioneer')
const bp = require('./ethereum_implementations/ClearBidderPool')

const bidderPool = new bp(event);
const auctioneer = new ac(event);

auctioneer.newAuctionRequest();