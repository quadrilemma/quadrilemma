
const env = require("../environment.js");
const bidderClear = require('./ClearBidder');
const {toBuffer} = require("ethereumjs-util");
const Web3 = require('web3');
const {asciiToHex, randomHex} = require("web3-utils");
const OpenAuction = require('./OpenAuction.json')
const N = 3;

module.exports = function (event) {


    const web3 = new Web3(new Web3.providers.HttpProvider(env.InfuraUrlProviderHTTPS));
    const web3ws = new Web3(new Web3.providers.WebsocketProvider(env.InfuraProviderWSS));

    console.log("Start BidderPool");
    let bidderPool = [];

    for (let i = 0; i < N; i++) {
        bidderPool.push(new bidderClear(web3ws, i));
    }

    event.on('new_auction', (address, period)=>{
        try{
            console.log("RICEVUTO");
            bidderPool[0].sendBid(address, 0);
            let i = 1
            let OpenAuctionContract = new web3ws.eth.Contract(OpenAuction.abi, address);

            OpenAuctionContract.events.HighestBidIncreased().on('data', (event) => {
                if (i < N) {
                    bidderPool[0].sendBid(address, i);
                    i += 1;
                }
                if (i === N){
                    setTimeout(() => {
                        web3ws.currentProvider.disconnect();
                    }, 30000)

                }
            });
        } catch (e) {
            console.log(e);
            web3ws.currentProvider.disconnect();
        }
    });


}

