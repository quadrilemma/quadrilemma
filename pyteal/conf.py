from pyteal import *


def approval_program_auction_contract():
    failed = Return(Int(0))


    on_creation = Seq([
        App.globalPut(Bytes("Auctioneer"), Txn.sender()),
        Assert(Txn.application_args.length() == Int(1)),
        # Assert(Txn.application_args[3] == Txn.sender()),
        # Assert(Txn.sender() == Txn.application_args[3]),
        App.globalPut(Bytes("AuctionBegin"), Global.latest_timestamp()),
        App.globalPut(Bytes("AuctionEnd"), Global.latest_timestamp() + Int(240)),
        App.globalPut(Bytes("CommitEnd"), Global.latest_timestamp() + Int(120)),
        App.globalPut(Bytes("AuctionedToken"), Btoi(Txn.application_args[0])),  # 1 byte
        App.globalPut(Bytes("MaxBid"), Int(0)),  # or base
        App.globalPut(Bytes("MaxBidder"), Bytes("NONE")),
        Return(Int(1))
    ])

    is_creator = Gtxn[0].sender() == App.globalGet(Bytes("Auctioneer"))

    on_update = Seq([
        Assert(is_creator),
        Assert(Global.group_size() == Int(1)),
        Assert(Txn.application_args.length() == Int(2)),
        App.globalPut(Bytes("EscrowContract"), Txn.application_args[1]),
        Return(Int(1))
    ])

    on_sc = Seq([
        Assert(And(
            is_creator,
            Txn.application_args.length() == Int(3),
            )),
        App.globalPut(Txn.application_args[1], Btoi(Txn.application_args[2])),
        Return(Int(1))
    ])

    # get_bid_of_sender = App.globalGetEx(App.id(), Gtxn[0].sender())

    max_bid = App.globalGet(Bytes("MaxBid"))
    max_bidder = App.globalGet(Bytes("MaxBidder"))
    escrow_contract = App.globalGet(Bytes("EscrowContract"))
    auctioneer = App.globalGet(Bytes("Auctioneer"))
    sc = App.globalGetEx(Int(0), Gtxn[0].application_args[1])

    on_commit = Seq([
        sc,
        Assert(
            And(
                sc.hasValue(),
                Global.latest_timestamp() <= App.globalGet(Bytes("CommitEnd")),
                Global.latest_timestamp() >= App.globalGet(Bytes("AuctionBegin")),
                sc.value() == Gtxn[1].application_id(), # need to check that the other call is of a legit smart contract
                Gtxn[2].amount() == Int(1000),
                Gtxn[2].receiver() == escrow_contract,
                Gtxn[2].sender() == Gtxn[1].sender() == Gtxn[0].sender(),

            )
        ),
        Return(Int(1))
    ])

    on_reveal = Seq([
        sc,
        Assert(
            And(
                sc.hasValue(),
                # Global.latest_timestamp() >= App.globalGet(Bytes("CommitEnd")), // TODO wait more this
                Global.latest_timestamp() <= App.globalGet(Bytes("AuctionEnd")),
                sc.value() == Gtxn[1].application_id(),
                Gtxn[2].amount() == Int(1000),
                Gtxn[2].sender() == escrow_contract,
                Gtxn[1].sender() == Gtxn[0].sender(),
                )
        ),
        Return(Int(1))
    ])

    bid = App.globalGetEx(Int(1), Gtxn[0].sender())
    on_bid = Seq([
        bid,
        sc,

        If(max_bid == Int(0), Assert(Global.group_size() == Int(2)),
           Assert(
               And(Global.group_size() == Int(3),
                   Gtxn[2].receiver() == max_bidder,
                   Gtxn[2].sender() == escrow_contract,
                   Gtxn[2].amount() == max_bid))),

        Assert(

                And(
                    # Check whether it is in the right interval time
                    # Global.latest_timestamp() >= App.globalGet(Bytes("CommitEnd")), // TODO wait more this
                    Global.latest_timestamp() <= App.globalGet(Bytes("AuctionEnd")),
                    Gtxn[1].amount() > max_bid,
                    Gtxn[0].application_args.length() == Int(2),
                    sc.hasValue(),
                    bid.hasValue(),
                    Btoi(bid.value()) == Gtxn[1].amount(),
                    Gtxn[0].sender() == Gtxn[1].sender(),
                    Gtxn[1].receiver() == escrow_contract
                ),
        ),

        App.globalPut(Bytes("MaxBidder"), Gtxn[1].sender()),
        App.globalPut(Bytes("MaxBid"), Gtxn[1].amount()),
        Return(Int(1))
    ])

    on_finish = Seq([

        Assert(And(
            Global.latest_timestamp() > App.globalGet(Bytes("AuctionEnd")),
            Global.group_size() == Int(3),
            Gtxn[1].type_enum() == Int(4),
            Gtxn[2].amount() == max_bid,
            Gtxn[0].sender() == max_bidder,
            # Gtxn[0].sender() == Gtxn[1].receiver(), # TODO this not work

            # Gtxn[1].receiver() == Gtxn[0].application_args[1],
            # Gtxn[0].application_args[1] == max_bidder,
            # Gtxn[1].receiver() == max_bidder, # TODO this not work
            Gtxn[2].receiver() == auctioneer,
            Gtxn[1].sender() == Gtxn[2].sender(),
            Gtxn[1].sender() == escrow_contract,
        )),
        Return(Int(1))
    ])



    program = Cond(
        [Txn.application_id() == Int(0), on_creation],
        [Gtxn[0].on_completion() == OnComplete.DeleteApplication, Return(is_creator)],
        [Gtxn[0].on_completion() == OnComplete.UpdateApplication, Return(is_creator)],
        [Gtxn[0].on_completion() == OnComplete.CloseOut, Return(is_creator)],
        [Gtxn[0].on_completion() == OnComplete.OptIn, Return(is_creator)],
        [Gtxn[0].application_args[0] == Bytes("bid"), on_bid],
        [Gtxn[0].application_args[0] == Bytes("escrow"), on_update],
        [Gtxn[0].application_args[0] == Bytes("commit"), on_commit],
        [Gtxn[0].application_args[0] == Bytes("reveal"), on_reveal],
        [Gtxn[0].application_args[0] == Bytes("finish"), on_finish],
        [Gtxn[0].application_args[0] == Bytes("sc"), on_sc]
    )
    return program


def approval_program_committing_contract():
    failed = Return(Int(0))

    on_creation = Seq([
        App.globalPut(Bytes("Auctioneer"), Txn.sender()),
        Assert(Txn.application_args.length() == Int(1)),
        App.globalPut(Bytes("AuctionContract"), Btoi(Txn.application_args[0])),
        Return(Int(1))
    ])

    is_creator = Gtxn[0].sender() == App.globalGet(Bytes("Auctioneer"))
    auction_contract = App.globalGet(Bytes("AuctionContract"))
    # get_bid_of_sender = App.globalGetEx(App.id(), Gtxn[0].sender())
    escrow_contract = App.globalGetEx(Int(1), Bytes("EscrowContract"))
    bidding_begin = App.globalGetEx(Int(1), Bytes("AuctionBegin"))
    bidding_end = App.globalGetEx(Int(1), Bytes("CommitEnd"))

    on_commit = Seq([
        escrow_contract,
        bidding_begin,
        bidding_end,
        If(Not(escrow_contract.hasValue()), failed),
        If(Not(bidding_begin.hasValue()), failed),
        If(Not(bidding_end.hasValue()), failed),


        Assert(
            And(
                Global.group_size() == Int(3),
                Global.latest_timestamp() >= bidding_begin.value(),
                # Check whether it is in the right interval time
                Global.latest_timestamp() <= bidding_end.value(),
                Gtxn[0].application_id() == auction_contract,
                Gtxn[1].application_args.length() == Int(2),
                Gtxn[0].sender() == Gtxn[1].sender(),
                Gtxn[2].receiver() == escrow_contract.value()
                ),
        ),

        App.globalPut(Gtxn[1].sender(), Gtxn[1].application_args[1]),
        Return(Int(1))
    ])

    commitment = App.globalGetEx(Int(0), Gtxn[1].sender())

    on_reveal = Seq([
        escrow_contract,
        commitment,
        bidding_begin,
        bidding_end,
        If(Not(commitment.hasValue()), failed),
        If(Not(escrow_contract.hasValue()), failed),
        If(Not(bidding_begin.hasValue()), failed),
        If(Not(bidding_end.hasValue()), failed),

        Assert(
            And(
                Global.group_size() == Int(3),
                # Global.latest_timestamp() > bidding_end.value(), TODO this fix
                Gtxn[1].application_args.length() == Int(2),
                Gtxn[0].application_id() == auction_contract,
                Gtxn[0].sender() == Gtxn[1].sender(),
                Gtxn[2].sender() == escrow_contract.value(),
                Sha256(Gtxn[1].application_args[1]) == commitment.value(),
            ),
        ),

        App.globalPut(Gtxn[1].sender(), Gtxn[1].application_args[1]),
        Return(Int(1))
    ])


    program = Cond(
        [Txn.application_id() == Int(0), on_creation],
        [Gtxn[0].on_completion() == OnComplete.DeleteApplication, Return(is_creator)],
        [Gtxn[0].on_completion() == OnComplete.UpdateApplication, Return(is_creator)],
        [Gtxn[0].on_completion() == OnComplete.CloseOut, Return(is_creator)],
        [Gtxn[0].on_completion() == OnComplete.OptIn, Return(is_creator)],
        [Gtxn[0].application_args[0] == Bytes("commit"), on_commit],
        [Gtxn[0].application_args[0] == Bytes("reveal"), on_reveal]
    )
    return program

def clear_state_program():
    is_creator = Txn.sender() == App.globalGet(Bytes("Auctioneer"))
    program = Return(is_creator)

    return program

def committing_clear_state_program():
    program = Return(Int(1))

    return program

if __name__ == "__main__":
    with open('conf_application_stateful.teal', 'w') as f:
        compiled = compileTeal(approval_program_auction_contract(), Mode.Application)
        f.write(compiled)

    with open('committing_application_stateful.teal', 'w') as f:
        compiled = compileTeal(approval_program_committing_contract(), Mode.Application)
        f.write(compiled)

    with open('conf_clear_state.teal', 'w') as f:
        compiled = compileTeal(clear_state_program(), Mode.Application)
        f.write(compiled)

    with open('committing_clear_state.teal', 'w') as f:
        compiled = compileTeal(committing_clear_state_program(), Mode.Application)
        f.write(compiled)
