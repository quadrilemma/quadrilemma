const env = require("../environment.js");
const {toBuffer} = require("ethereumjs-util");
const Web3 = require('web3');
const {asciiToHex, randomHex} = require("web3-utils");
const ConfidentialAuction = require('./ConfidentialAuction.json')

module.exports = function (event) {


    const web3 = new Web3(new Web3.providers.HttpProvider(env.InfuraUrlProviderHTTPS));
    const web3ws = new Web3(new Web3.providers.WebsocketProvider(env.InfuraProviderWSS));

    const sk = env.ethereum.auctioneer.sK;
    const account = web3.eth.accounts.privateKeyToAccount(sk);
    console.log("Auctioneer address: " + account.address);
    const biddingPeriod = 120;
    const revealPeriod = 240;
    const deposit = 10000;

    const wallet = web3ws.eth.accounts.wallet.add(account);
    const keystore = wallet.encrypt(randomHex(32));

    let ConfidentialAuctionContract;

    this.newAuctionRequest =  async function () {
        newAuction();
    }

    function newAuction(){

        ConfidentialAuctionContract = new web3ws.eth.Contract(ConfidentialAuction.abi);
        ConfidentialAuctionContract.options.data = '0x'+ConfidentialAuction.bytecode.object;

        let newDeploy = ConfidentialAuctionContract.deploy({arguments: [biddingPeriod, revealPeriod, account.address, deposit]})
        newDeploy.estimateGas({from: account.address},(err, gas) =>{
            if (err) console.log(err);

            newDeploy.send({
                from: account.address,
                gas: Math.floor(gas*1.5),
            })
                .once('transactionHash', txhash => {
                    console.log(`Mining ContractCreation transaction ...`);
                    console.log(`https://ropsten.etherscan.io/tx/${txhash}`);
                })
                .then(function(newContractInstance) {

                    ConfidentialAuctionContract = new web3ws.eth.Contract(ConfidentialAuction.abi, newContractInstance.options.address)// instance with the new contract address
                    console.log(ConfidentialAuctionContract.options.address);
                    console.log("============== NEW EVENT ==============");
                    event.emit('new_auction', ConfidentialAuctionContract.options.address, deposit, biddingPeriod);
                    console.log("============== SET TIMEOUT ==============");
                    let interval = setInterval(() => {
                        console.log("Ending Auction....")
                        const newReq =  ConfidentialAuctionContract.methods.auctionEnd();
                        newReq.estimateGas({from: account.address}).then((gas)=> {
                            newReq.send({from: account.address,gas: Math.trunc(gas*(1.5))}).
                            once('transactionHash', txhash => {
                                console.log(`Mining auctionEnd transaction ...`);
                                console.log(`https://ropsten.etherscan.io/tx/${txhash}`);
                            }).then((r) => {
                                console.log("Highest bid is " + r.events.AuctionEnded.returnValues.winner + "with the value: "+ r.events.AuctionEnded.returnValues.highestBid);
                                clearInterval(interval);
                                web3ws.currentProvider.disconnect();
                            }).catch((e) => {
                                console.log(e)
                                web3ws.currentProvider.disconnect()});
                        });
                    }, (revealPeriod+60)*1000);

                }).catch((e) => {
                console.log(e)
                web3ws.currentProvider.disconnect()})
        });

    }
}